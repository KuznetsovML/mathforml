# mathforml
## Занятие 0

 [Ссылка на блокнот с занятия](https://1drv.ms/u/s!AtKOK2CTXZpFgSj5tbnTFFyWt75T)\
 [Инструмент для построения графиков](https://www.desmos.com/?lang=ru)\
 Что посмотреть\почитать:\
 [Про функциюю, очень эмоционально, но понятно](https://www.youtube.com/watch?v=57T9Vx_tzG4&ab_channel=ArturSharifov)\
 [Графическое объяснение производной, советую начать с этой статьи](http://mathprofi.ru/opredelenie_proizvodnoi_smysl_proizvodnoi.html)\
 [Более математичное объяснение производной, советую этот канал](https://www.youtube.com/watch?v=sSnyhOXFLqc&ab_channel=%D0%91%D0%BE%D1%80%D0%B8%D1%81%D0%A2%D1%80%D1%83%D1%88%D0%B8%D0%BD)\
 [Если хочешь понять, что за табличка производных и как ее использовать](https://www.youtube.com/watch?v=7Z6QZeagSZU&ab_channel=TutorOnline-%D1%83%D1%80%D0%BE%D0%BA%D0%B8%D0%B4%D0%BB%D1%8F%D1%88%D0%BA%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2)
 
 
 
 ## Домашнее задание.
 
 Провести анализ 3 функций:
 ```math
 1. $$f(x) = max(0.1x, x)$$ (в ML ее называют LeakyRelu)
 2. $f(x) = 1 + e^x + x^2$
 3. $f(x) = /frac{exp(x)-exp(‑x)}{exp(x)+exp(‑x)}$ (в ML ее называют гиперболический тангенс)
 ```

 
Анализ включает: область определения, область значений, возрастание и убывание, max и min
